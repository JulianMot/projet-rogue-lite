import pygame
from pygame.locals import *
import re
import random
from sprites import *
from settings import *

red = (200, 0, 0)
green = (0, 200, 0)
black = (0, 0, 0)

bright_red = (255, 0, 0)
bright_green = (0, 255, 0)

nombre_sprite_cote = 16
taille_sprite = 62


class App:

    def __init__(self):
        pygame.init()
        pygame.mixer.init()
        pygame.display.set_caption("Cave")
        self.size = self.weight, self.height = WIDTH, HEIGHT
        self._display_surf = pygame.display.set_mode(self.size, DOUBLEBUF)
        self.clock = pygame.time.Clock()
        self._running = True
        self.menu = True
        self.victory = False
        self.map = Map()
        self.actual_level_x = 0
        self.actual_level_y = 0
        self.joueur = Joueur(self)
        self.niveau = Niveau("Start")


    def on_menu(self):
        self._display_surf.blit(pygame.image.load("images/background.jpg").convert(), (0, 0))
        bouton_jouer = Button("Jouer", 250, 450, 100, 50, green)
        bouton_quitter = Button("Quitter", 650, 450, 100, 50, red)
        bouton_quitter.draw(self._display_surf)
        bouton_jouer.draw(self._display_surf)
        pygame.display.flip()
        choice = 0
        while choice == 0:
            for event in pygame.event.get():
                mouse = pygame.mouse.get_pos()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if bouton_jouer.isOver(mouse):
                        choice = 1
                    elif bouton_quitter.isOver(mouse):
                        choice = 2
        if choice == 1:
            return True
        if choice == 2:
            return False

    def on_init(self):
        self.map.generate_Map()
        self.niveau = Niveau(self.map.map[0][0])
        self.all_sprites = pygame.sprite.Group()
        if self.on_menu():
            self.all_sprites.add(self.joueur)
            self.niveau.generer()
            for block in self.niveau.solid_blocks:
                self.all_sprites.add(block)
        else:
            self._running = False

    def on_event(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self._running = False
                if event.key == pygame.K_SPACE:
                    self.joueur.jump()
                if event.key == pygame.K_LEFT and self.joueur.position.x == 0:
                    self.actual_level_y -= 1
                    self.joueur.position.x = WIDTH
                if event.key == pygame.K_RIGHT and self.joueur.position.x == WIDTH:
                    self.actual_level_y += 1
                    self.joueur.position.x = 0
                if event.key == pygame.K_UP and self.joueur.position.y <= 0:
                    self.actual_level_x -= 1
                    self.joueur.position.y = HEIGHT
                if event.key == pygame.K_DOWN and self.joueur.position.y == HEIGHT:
                    self.actual_level_x += 1
                    self.joueur.position.y = 0

    def on_loop(self):
        self.all_sprites.empty()
        self.niveau = Niveau(self.map.map[self.actual_level_x][self.actual_level_y])
        self.niveau.generer()
        for block in self.niveau.solid_blocks:
            self.all_sprites.add(block)
        self.all_sprites.add(self.joueur)
        self.all_sprites.update()
        if self.joueur.velocity.y > 0:
            hits = pygame.sprite.spritecollide(self.joueur, self.niveau.solid_blocks, False)
            if hits:
                if self.joueur.position.y < hits[0].rect.bottom:
                    self.joueur.position.y = hits[0].rect.top
                    self.joueur.velocity.y = 0
                for block in hits:
                    if block.type == "3":
                        self.victory = True

    def on_render(self):
        if self.victory:
            self._display_surf.blit(pygame.image.load("images/background.jpg").convert(), (0, 0))
            victoire_info = Button("Victoire !",  WIDTH // 2, HEIGHT // 2, WIDTH // 3, HEIGHT // 5, green)
            victoire_info.draw(self._display_surf)
        else:
            self._display_surf.fill(Color(0, 0, 0))
            self.niveau.solid_blocks.draw(self._display_surf)
            self.all_sprites.draw(self._display_surf)
        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False
        while self._running:
            self.clock.tick(FPS)
            self.on_event()
            self.on_loop()
            self.on_render()
        self.on_cleanup()


class Button:
    def __init__(self, text, x, y, width, height, color):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text

    def draw(self, win, outline=None):
        # Call this method to draw the button on the screen
        if outline:
            pygame.draw.rect(win, outline, (self.x - 2, self.y - 2, self.width + 4, self.height + 4), 0)

        pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.height), 0)

        if self.text != '':
            font = pygame.font.SysFont('comicsans', 40)
            text = font.render(self.text, 1, (0, 0, 0))
            win.blit(text, (
                self.x + (self.width / 2 - text.get_width() / 2), self.y + (self.height / 2 - text.get_height() / 2)))

    def isOver(self, pos):
        # Pos is the mouse position or a tuple of (x,y) coordinates
        if self.x < pos[0] < self.x + self.width:
            if self.y < pos[1] < self.y + self.height:
                return True
        return False


class Niveau:

    def __init__(self, fichier):
        self.fichier = "Niveaux/" + fichier + ".txt"
        self.structure = 0
        self.solid_blocks = pygame.sprite.Group()
        self.air_blocks = pygame.sprite.Group()

    def generer(self):
        """Méthode permettant de générer le niveau en fonction du fichier.
        On crée une liste générale, contenant une liste par ligne à afficher
        puis on créer un groupe de sprite Block à afficher"""
        # On ouvre le fichier
        with open(self.fichier, "r") as fichier:
            structure_niveau = []
            # On parcourt les lignes du fichier
            for ligne in fichier:
                ligne_niveau = []
                # On parcourt les sprites (lettres) contenus dans le fichier
                for sprite in ligne:
                    # On ignore les "\n" de fin de ligne
                    if sprite != '\n':
                        # On ajoute le sprite à la liste de la ligne
                        ligne_niveau.append(sprite)
                # On ajoute la ligne à la liste du niveau
                structure_niveau.append(ligne_niveau)
            # On sauvegarde cette structure
            self.structure = structure_niveau

            # On parcourt la liste du niveau
            num_ligne = 0
            for ligne in self.structure:
                # On parcourt les listes de lignes
                num_case = 0
                for sprite in ligne:
                    # On calcule la position réelle en pixels
                    x = num_case * WIDTH // 16
                    y = num_ligne * HEIGHT // 9
                    block = Block(x, y, sprite)
                    if sprite == "1" or sprite == "2" or sprite == "3":
                        self.solid_blocks.add(block)
                    else:
                        self.air_blocks.add(block)
                    num_case += 1
                num_ligne += 1


class Map:

    def __init__(self):
        self.dim = 10
        self.levels = ["0000", "0001", "0010", "0011", "0100", "0101",
                       "0000", "0001", "0010", "0011", "0100", "0101",
                       "0110", "1000", "1001", "1010", "1100",
                       "0110", "1000", "1001", "1010", "1100",
                       "0000", "0001", "0010", "0011", "0100", "0101",
                       "0000", "0001", "0010", "0011", "0100", "0101",
                       "0110", "1000", "1001", "1010", "1100",
                       "0110", "1000", "1001", "1010", "1100",
                       "0000", "0001", "0010", "0011", "0100", "0101",
                       "0000", "0001", "0010", "0011", "0100", "0101",
                       "0110", "1000", "1001", "1010", "1100",
                       "0110", "1000", "1001", "1010", "1100",
                       "1111", "0111", "1011", "1101", "1110"]   # Cul de sac
        self.special_levels = ["Start", "End"]
        self.map = [["0" for i in range(self.dim)] for j in range(self.dim)]

    def generate_Map(self):
        for i in range(self.dim):
            for j in range(self.dim):
                if i == 0:
                    exp = re.compile(r'^.{1}1')
                    matchs = list(filter(exp.search, self.levels))
                    if j == 0:
                        self.map[i][j] = self.special_levels[0]
                    elif j == 1:
                        exp = re.compile(r"^.{0}0")
                        matchs = list(filter(exp.search, matchs))
                        self.map[i][j] = random.choice(matchs)
                    else:
                        prec = re.compile(r'^.{3}0')
                        niveau_gauche_ouvert = re.search(prec, self.map[i][j - 1])
                        if niveau_gauche_ouvert == None:
                            if j == self.dim - 1:
                                self.map[i][j] = "1111"
                            else:
                                exp = re.compile(r"^.{0}1")
                                matchs = list(filter(exp.search, matchs))
                                self.map[i][j] = random.choice(matchs)
                        else:
                            if j == self.dim - 1:
                                exp = re.compile(r"^.{3}1")
                            else:
                                exp = re.compile(r"^.{0}0")
                            matchs = list(filter(exp.search, matchs))
                            self.map[i][j] = random.choice(matchs)
                elif i == self.dim - 1:
                    matchs = self.levels
                    prev = re.compile(r'^.{2}0')
                    niveau_haut_ouvert = re.search(prev, self.map[i - 1][j])
                    prec = re.compile(r'^.{3}0')
                    niveau_gauche_ouvert = re.search(prec, self.map[i][j - 1])
                    exp = re.compile(r"^.{2}1")
                    matchs = list(filter(exp.search, matchs))
                    if j == self.dim - 1:
                        self.map[i][j] = "End"
                    elif j == self.dim - 2:
                        exp = re.compile(r"^.{3}0")
                        matchs = list(filter(exp.search, matchs))
                        if niveau_gauche_ouvert == None:
                            exp = re.compile(r'^.{0}1')
                        else:
                            exp = re.compile(r'^.{0}0')
                            matchs = list(filter(exp.search, matchs))
                            self.map[i][j] = random.choice(matchs)
                    elif j == 0:
                        exp = re.compile(r'^.{0}1')
                        matchs = list(filter(exp.search, matchs))
                        if niveau_haut_ouvert == None:
                            exp = re.compile(r'^.{1}1')
                        else:
                            exp = re.compile(r'^.{1}0')
                        matchs = list(filter(exp.search, matchs))
                        self.map[i][j] = random.choice(matchs)
                    else:
                        if niveau_haut_ouvert == None:
                            exp = re.compile(r'^.{1}1')
                            matchs = list(filter(exp.search, matchs))
                            if niveau_gauche_ouvert == None:
                                exp = re.compile(r'^.{0}1')
                            else:
                                exp = re.compile(r'^.{0}0')
                                matchs = list(filter(exp.search, matchs))
                        else:
                            exp = re.compile(r'^.{1}0')
                            matchs = list(filter(exp.search, matchs))
                            if niveau_gauche_ouvert == None:
                                exp = re.compile(r'^.{0}1')
                            else:
                                exp = re.compile(r'^.{0}0')
                            matchs = list(filter(exp.search, matchs))
                        self.map[i][j] = random.choice(matchs)
                else:
                    matchs = self.levels
                    haut = re.compile(r'^.{2}0')
                    niveau_haut_ouvert = re.search(haut, self.map[i - 1][j])
                    prec = re.compile(r'^.{3}0')
                    niveau_gauche_ouvert = re.search(prec, self.map[i][j - 1])
                    if j == 0:
                        exp = re.compile(r'^.{0}1')
                        matchs = list(filter(exp.search,matchs))
                        if niveau_haut_ouvert == None:
                            exp = re.compile(r'^.{1}1')
                        else:
                            exp = re.compile(r'^.{1}0')
                        matchs = list(filter(exp.search,matchs))
                        self.map[i][j] = random.choice(matchs)
                    elif j == self.dim - 1:
                        exp = re.compile(r'^.{3}1')
                        matchs = list(filter(exp.search, matchs))
                        if i == self.dim - 2:
                            exp = re.compile(r'^.{2}1')
                            matchs = list(filter(exp.search, matchs))
                        if niveau_haut_ouvert == None:
                            exp = re.compile(r'^.{1}1')
                        else:
                            exp = re.compile(r'^.{1}0')
                        matchs = list(filter(exp.search, matchs))
                        if niveau_gauche_ouvert == None:
                            exp = re.compile(r"^.{0}1")
                        else:
                            exp = re.compile(r"^.{0}0")
                        matchs = list(filter(exp.search, matchs))
                        self.map[i][j] = random.choice(matchs)
                    else:
                        if niveau_haut_ouvert == None:
                            exp = re.compile(r'^.{1}1')
                        else:
                            exp = re.compile(r'^.{1}0')
                        matchs = list(filter(exp.search, matchs))
                        if niveau_gauche_ouvert == None:
                            exp = re.compile(r"^.{0}1")
                        else:
                            exp = re.compile(r"^.{0}0")
                        matchs = list(filter(exp.search, matchs))
                        self.map[i][j] = random.choice(matchs)
        for ligne in self.map:
            print(ligne)


if __name__ == "__main__":
    theApp = App()
    theApp.on_execute()
