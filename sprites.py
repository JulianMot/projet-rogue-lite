# Fichier pour tous les Sprites
import pygame
from main import *
from settings import *

vec = pygame.math.Vector2


class Joueur(pygame.sprite.Sprite):
    def __init__(self, app):
        pygame.sprite.Sprite.__init__(self)
        self.app = app
        self.immobile = True
        self.en_marche = False
        self.jumpping = False
        self.a_droite = True
        self.current_frame = 0
        self.last_update = 0
        self.load_images()
        self.image = self.idleSprites_r[0]
        self.image = pygame.transform.scale(self.image, (WIDTH // 16, HEIGHT // 9))
        self.rect = self.image.get_rect()
        self.rect.center = (WIDTH / 2, HEIGHT / 2)
        self.position = vec(WIDTH / 2, HEIGHT / 2)
        self.velocity = vec(0, 0)
        self.acceleration = vec(0, 0)

    def load_images(self):

        self.idleSprites_r = [pygame.image.load("images/soldat/Idle (1).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (2).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (3).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (4).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (5).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (6).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (7).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (8).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (9).png").convert_alpha(),
                       pygame.image.load("images/soldat/Idle (10).png").convert_alpha()]

        self.idleSprites_l = []
        for frame in self.idleSprites_r:
            self.idleSprites_l.append(pygame.transform.flip(frame, True, False))

        self.deadSprites_r = [pygame.image.load("images/soldat/Dead (1).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (2).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (3).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (4).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (5).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (6).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (7).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (8).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (9).png").convert_alpha(),
                         pygame.image.load("images/soldat/Dead (10).png").convert_alpha()]

        self.deadSprites_l = []
        for frame in self.deadSprites_r:
            self.deadSprites_l.append(pygame.transform.flip(frame, True, False))

        self.attackSprites_r = [pygame.image.load("images/soldat/Attack (1).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (2).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (3).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (4).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (5).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (6).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (7).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (8).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (9).png").convert_alpha(),
                           pygame.image.load("images/soldat/Attack (10).png").convert_alpha()]

        self.attackSprites_l = []
        for frame in self.attackSprites_r:
            self.attackSprites_l.append(pygame.transform.flip(frame, True, False))

        self.jumpSprites_r = [pygame.image.load("images/soldat/Jump (1).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (2).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (3).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (4).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (5).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (6).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (7).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (8).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (9).png").convert_alpha(),
                         pygame.image.load("images/soldat/Jump (10).png").convert_alpha()]

        self.jumpSprites_l = []
        for frame in self.jumpSprites_r:
            self.jumpSprites_l.append(pygame.transform.flip(frame, True, False))

        self.runSprites_r = [pygame.image.load("images/soldat/Run (1).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (2).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (3).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (4).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (5).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (6).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (7).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (8).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (9).png").convert_alpha(),
                        pygame.image.load("images/soldat/Run (10).png").convert_alpha()]

        self.runSprites_l = []
        for frame in self.runSprites_r:
            self.runSprites_l.append(pygame.transform.flip(frame, True, False))

    def jump(self):
        self.rect.x += 1
        hits = pygame.sprite.spritecollide(self, self.app.niveau.solid_blocks, False)
        self.rect.x -= 1
        if hits:
            self.velocity.y = JUMP
            self.jumpping = True
        self.jumpping = False

    def update(self):
        self.animate()
        self.acceleration = vec(0, GRAVITY)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.acceleration.x = - HORIZONTAL_SPEED
            self.a_droite = False
            self.app.niveau.solid_blocks
        if keys[pygame.K_RIGHT]:
            self.acceleration.x = HORIZONTAL_SPEED
            self.a_droite = True

        self.acceleration.x += self.velocity.x * FRICTION
        self.velocity += self.acceleration
        self.position += self.velocity + 0.5 * self.acceleration
        if self.position.x > WIDTH:
            self.position.x = WIDTH
        elif self.position.x < 0:
            self.position.x = 0
        if self.position.y > HEIGHT:
            self.position.y = HEIGHT
        elif self.position.y < 0:
            self.position.y = 0

        self.rect.midbottom = self.position

        self.en_marche = (not abs(self.velocity.x) == 0)

        if abs(self.velocity.x) < 1:
            self.velocity.x = 0


    def animate(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > 100:
            if self.a_droite:
                if not self.jumpping and not self.en_marche:
                    self.current_frame = (self.current_frame + 1) % len(self.idleSprites_r)
                    self.last_update = now
                    self.image = self.idleSprites_r[self.current_frame]
                else:
                    self.current_frame = (self.current_frame + 1) % len(self.runSprites_r)
                    self.last_update = now
                    self.image = self.runSprites_r[self.current_frame]
            else:
                if not self.jumpping and not self.en_marche:
                    self.current_frame = (self.current_frame + 1) % len(self.idleSprites_l)
                    self.last_update = now
                    self.image = self.idleSprites_l[self.current_frame]
                else:
                    self.current_frame = (self.current_frame + 1) % len(self.runSprites_l)
                    self.last_update = now
                    self.image = self.runSprites_l[self.current_frame]


        self.image = pygame.transform.scale(self.image, (WIDTH // 16, HEIGHT // 9))

class Block(pygame.sprite.Sprite):

    def __init__(self, x, y, type_block):
        pygame.sprite.Sprite.__init__(self)
        self.w = WIDTH // 16
        self.h = HEIGHT // 9
        self.type = type_block
        if type_block == "1":
            # Chargement des images (seule celle d'arrivée contient de la transparence)
            self.image = pygame.image.load("images/Indestructible_block.png").convert_alpha()
        elif type_block == "2":
            self.image = pygame.image.load("images/Destructible_block.png").convert_alpha()
        elif type_block == "3":
            self.image = pygame.image.load("images/Chest_block.png").convert_alpha()
        else:
            self.image = pygame.image.load("images/Empty_block.png").convert_alpha()

        self.image = pygame.transform.scale(self.image, (self.w, self.h))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
