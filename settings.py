#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 18 09:48:10 2020

@author: julian
"""

WIDTH = 1280
HEIGHT = 720
FPS = 60

FRICTION = -0.3
GRAVITY = 4
JUMP =  - HEIGHT // 20
HORIZONTAL_SPEED = WIDTH // 120